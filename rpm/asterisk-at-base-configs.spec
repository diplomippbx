Name: asterisk-at-base-configs
Summary: AT Asterisk config files
Version: 0.1
Release: alt10
License: GPL
Group: System/Servers

BuildArch: noarch

Packager: Vitaliy Kolodinsky <kolodinsky@gmail.com>

Source: %name-%version.tar.gz

Requires(pre): asterisk-base

%description
%summary

%prep
%setup -c
%build
%install
mkdir -p %buildroot%_sysconfdir/asterisk
ls -l
for s in *.*; do
        install -D -m 750 "$s" %buildroot%_sysconfdir/asterisk
done

%post
cd %_sysconfdir/asterisk/
/bin/tar -zxf %_sysconfdir/asterisk/configs.tar.gz
/bin/chown _asterisk:pbxadmin %_sysconfdir/asterisk/*.*
/bin/chmod 0460 %_sysconfdir/asterisk/*.*
%files
%dir %attr(2770,_asterisk,pbxadmin) %_sysconfdir/asterisk/
%config %attr(0460,_asterisk,pbxadmin) %_sysconfdir/asterisk/*.*
