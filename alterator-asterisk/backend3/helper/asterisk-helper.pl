#!/usr/bin/perl -w

use Data::Dumper;
use Asterisk::config;
use strict;
use warnings;
use Getopt::Long;


use vars qw / $opt_action $opt_fname $opt_section $opt_key $opt_value $opt_data /;
GetOptions("action=s","fname:s","section:s","key:s@"=>\$opt_key,"value:s@"=>\$opt_value,"data:s");

##        &&(defined($opt_section))&&(defined(@{$opt_key}))&&(defined(@{$opt_value})))

unless (($opt_action)&&(defined($opt_fname)))
{
    print STDERR "Not enough params\n";
}

if ($opt_action=~/listsections/i)
{
    listsections();
    exit 0;
}

if ($opt_action=~/addsectionvaluesxt/i)
{
    addsectionvaluesxt();
    exit 0;
}

if ($opt_action=~/addsectionvalues/i)
{
    addsectionvalues();
    exit 0;
}

if ($opt_action=~/getsectionvaluesxt/i)
{
    getsectionvaluesxt();
    exit 0;
}

if ($opt_action=~/getsectionvalues/i)
{
    getsectionvalues();
    exit 0;
}

if ($opt_action=~/delsection/i)
{
    delsection();
    exit 0;
}

#####################################################################

sub listsections {
    my $rc = new Asterisk::config(file=>$opt_fname);
    my $section_list = $rc->fetch_sections_list();
    foreach my $section( sort @{$section_list})
        {
        print "$section "
        }
}

sub addsectionvalues {
    my $rc = new Asterisk::config(file=>$opt_fname);
    $rc->assign_addsection(section=>$opt_section);
    for (my $i=@{$opt_key}-1; $i>-1;$i--)
        {
        $rc->assign_append(point=>'down',section=>$opt_section,data=>"${$opt_key}[$i]=${$opt_value}[$i]");
        }
    $rc->save_file(new_file=>$opt_fname);
}

sub addsectionvaluesxt{
    my $rc = new Asterisk::config(file=>$opt_fname);
    $rc->assign_addsection(section=>$opt_section);
    for (my $i=@{$opt_key}-1; $i>-1;$i--)
        {
        $rc->assign_append(point=>'down',section=>$opt_section,data=>"${$opt_key}[$i] => ${$opt_value}[$i]");
        }
    $rc->save_file(new_file=>$opt_fname);
}

sub getsectionvalues {
    my $rc = new Asterisk::config(file=>$opt_fname);
    my $key_ref = $rc->fetch_keys_hashref(section=>$opt_section);
	if ( $key_ref == '0') {	print "Section not found!\n";}
	else {
	    	for my $k1 ( sort keys %$key_ref )
	        {
	        for my $k2 ( @{$key_ref->{ $k1 }} )
	            {
	            print "$k1=$k2\n";
	            }
	        }
	}

}

sub getsectionvaluesxt {
    my $rc = new Asterisk::config(file=>$opt_fname);
    my $key_ref = $rc->fetch_keys_hashref(section=>$opt_section);
	if ( $key_ref == '0') {	print "Section not found!\n";}
	else {
    for my $k1 ( sort keys %$key_ref )
        {
        for my $k2 ( @{$key_ref->{ $k1 }} )
            {
            print "$k1 => $k2\n";
            }
        }
	}
}

sub delsection{
    my $rc = new Asterisk::config(file=>$opt_fname);
    $rc->assign_delsection(section=>$opt_section);
    $rc->save_file(new_file=>$opt_fname);
}
